//NOGIER LOIC
#include <iostream>
#include <stdlib.h>
using namespace std;

int exo01()
{
    int x;
    int i;
    cout << "Entrez une valeur supérieur à 1 : ";
    cin >> x;

    while (x < 1)
    {
        cout << "Merci d'entrez une valeur supérieur à 1 : ";
        cin >> x;
    }

    for (i = 2; i < x; i++)
    {
        if ((x % i) == 0)
            break;
    }

    if (i < x)
        cout << x << " est non premier" << endl;
    else
        cout << x << " est premier"  << endl;

    system("pause");
    return (0);
}

int ex06()
{

    int i = 0;
    int j = 0;
    int n = 0;
    do
    {
        cout << "Entrez n : ";
        cin >> n;
    }
    while ((n % 2) == 0 || (n < 0));

    for (i = 0; i < n; i++)
    {
        for (j = 0; j < n; j++)
        {
            if ((i == j) || (j == n -1 -i))
                cout << "X";
            else
                cout << " ";
        }
        cout << endl;
    }

    system("pause");
    return (0);
}

int main() {

    ex06();

    return (0);
}